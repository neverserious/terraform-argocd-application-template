variable "freeipa_host" { sensitive = true }
variable "freeipa_username" { sensitive = true }
variable "freeipa_password" { sensitive = true}
variable "freeipa_insecure" { default = false }
variable "kube_configpath" { sensitive = true }
variable "application_name" {}
variable "application_namespace" { default = "default" }
variable "argo_namespace" { default = "argocd" }
variable "argo_serverdestination" {
  default = "https://kubernetes.default.svc"
  sensitive = true
}
variable "repo_url" { sensitive = true }
variable "repo_username" { sensitive = true }
variable "repo_password" { sensitive = true }
variable "repo_path" {}
variable "repo_targetrevision" { default = "HEAD" }
variable "project_name" { default = "default" }
variable "project_type" { default = "git" }
variable "web_hostname" {}
variable "site_visibility" { default = "internal" }
variable "cloudflare_proxied" { default = true }
variable "cloudflare_token" { sensitive = true }
variable "dns_domain" { sensitive = true }
variable "dns_names" {}
variable "dns_recordtype" { default = "CNAME" }
variable "dns_target" { sensitive = true }
variable "group_params" {
  sensitive = true
  default   = "{}"
}
variable "project_params" {
  sensitive = true
  default   = "{}"
}
