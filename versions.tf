terraform {
  required_providers {
    freeipa = {
      source  = "camptocamp/freeipa"
      version = ">=0.7.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">=4.4.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">=2.20.0"
    }
  }
}
