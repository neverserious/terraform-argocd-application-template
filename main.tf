data "cloudflare_zone" "domain" {
  name = var.dns_domain
}

resource "kubernetes_secret" "repo" {
  metadata {
    name      = "repo-${var.application_name}"
    namespace = var.argo_namespace
    labels = {
      "argocd.argoproj.io/secret-type" = "repository"
    }
    annotations = {
      "managed-by" = "argocd.argoproj.io"
    }
  }
  data = {
    password = var.repo_password
    project  = var.project_name
    type     = var.project_type
    url      = var.repo_url
    username = var.repo_username
  }
  type = "Opaque"
}

# There should only be one of these, since this pipeline is only ever for a single deployed app.
resource "kubernetes_manifest" "argo_application" {
  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "Application"
    metadata = {
      name      = var.application_name
      namespace = var.argo_namespace
      finalizers = [
        "resources-finalizer.argocd.argoproj.io"
      ]
    }
    spec = {
      destination = {
        namespace = var.application_namespace
        server    = var.argo_serverdestination
      }
      project = var.project_name
      source = {
        path           = var.repo_path
        repoURL        = var.repo_url
        targetRevision = var.repo_targetrevision
        helm = {
          values                  = yamlencode(merge(yamldecode( var.group_params ),yamldecode( var.project_params )))
          ignoreMissingValueFiles = true
        }
      }
      syncPolicy = {
        syncOptions = [
          "CreateNamespace=true"
        ]
        automated = {
          selfHeal = true
          prune    = true
        }
      }
    }
  }
  computed_fields = [
    "spec.source.helm.parameters",
  ]
  field_manager {
    force_conflicts = true
  }
  depends_on = [kubernetes_secret.repo]
}

resource "cloudflare_record" "website" {
  for_each = var.site_visibility == "external" ? toset(var.dns_names) : []
  zone_id  = data.cloudflare_zone.domain.id
  name     = each.key
  value    = var.dns_target
  type     = var.dns_recordtype
  proxied  = var.cloudflare_proxied
  ttl      = 1
}

resource "freeipa_dns_record" "website" {
  for_each        = toset(var.dns_names)
  dnszoneidnsname = "${var.dns_domain}."
  idnsname        = each.key
  records         = [var.dns_target]
  type            = var.dns_recordtype
}
