provider "kubernetes" {
  config_path = var.kube_configpath
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}

provider "freeipa" {
  host     = var.freeipa_host
  username = var.freeipa_username
  password = var.freeipa_password
  insecure = var.freeipa_insecure
}
